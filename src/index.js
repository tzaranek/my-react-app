import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import App from './App';
import { ThemeProvider, createTheme, withStyles, Arwes, Puffs, Words, SoundsProvider, withSounds, createSounds, Button  } from 'arwes';
import { createPlayer } from 'arwes';
import { createLoader } from 'arwes';
import PlayerStatusPanel from './PlayerStatusPanel';

const loader = createLoader();

const images = [
  '/img/photo.jpg',
  '/img/wallpaper.jpg'
];
const sounds = [
  '/sound/song.mp3',
  '/sound/ringtone.mp3'
];

loader.load({ images, sounds }).then(() => {
  console.log('Resources were loaded.');
}, () => {
  console.error('Error when loading.');
});

const player = createPlayer(null, {
  sound: {  // Configuration to pass to howler.Howl class
    src: ['/sounds/error.mp3'],
    volume: 15
  },
  settings: {  // Custom settings
    oneAtATime: true  // Play only one audio at a time
  }
});

player.play();

const mySounds = {
  shared: { volume: 10, },  // Shared sound settings
  players: {  // The player settings
    click: {  // With the name the player is created
      sound: { src: ['/sounds/click.mp3'] }  // The settings to pass to Howler
    },
    typing: {
      sound: { src: ['/sounds/typing.mp3'] },
      settings: { oneAtATime: true }  // The custom app settings
    },
    deploy: {
      sound: { src: ['/sounds/deploy.mp3'] },
      settings: { oneAtATime: true }
    },
  }
};

const MyButton = withSounds()(props => (
  <Button
    onClick={function() {player.play();}}
    {...props}
  />
));

const App = () => (
  <ThemeProvider theme={createTheme()}>
    <SoundsProvider sounds={createSounds(mySounds)}>
      <Button animate>Click me</Button>
      <MyButton animate>Click 2</MyButton> 
      <PlayerStatusPanel></PlayerStatusPanel>
    </SoundsProvider>
  </ThemeProvider>
);

ReactDOM.render(<App />, document.querySelector('#root'));
