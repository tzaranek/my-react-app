import React from 'react';

class PlayerStatusPanel extends React.Component {
    render() {
      return (
        <div className="player-status-panel">
          <h1>Your alias {this.props.name}</h1>
          <ul>
            <li>Cash: </li>
            <li>Something1:</li>
            <li>Something2:</li>
          </ul>
        </div>
      );
    }
  }

export default PlayerStatusPanel